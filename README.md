# Albedo

Albedo is a series of libraries, companion utilities and web-apps for [Elite:
Dangerous](https://www.elitedangerous.com/), a game by [Frontier
Developments](https://www.frontier.co.uk/).

The primary goal of albedo is three-fold:

- Provide a wide variety of libraries for elite-related projects to use that:
  - Are correct, safe, efficient and high-quality
  - As small and simple as possible
  - Provide C interface through FFIs *and* javacript bindings through
    web-assembly
- Provide simple and easy-to-use apps to allow anyone to access all of albedo's
  libraries' features, either as:
  - A command-line utility
  - A local GUI application
  - A web-app
  - A web-hosted service with a safe, efficient and high-quality API
- Follow existing or provide standards, and allow and encourage tools
  inter-operability.

## Libraries

- `albedo-types`: Provides commonly used types.
- `albedo-traits`: Provides commonly used traits.
- `albedo-math`: Gather all the mathematical logic.
- `albedo-modules`: Ship modules data and related logic.
- `albedo-ships`: Ships data and related logic.

## Apps

- `albedo-cli`: A multi-purpose CLI to access (almost) all of albedo's features. 

## Hacking

This project is entirely written in [rust](https://www.rust-lang.org/). If you
intend to work on this project, we kindly ask you to comply to the
[rust code of conduct](https://www.rust-lang.org/policies/code-of-conduct).

There is two way you may want to hack this project: Using the traditional rust
workflow with rustup and cargo, or using [nix](nixos.org). You may want to use
a mix of both.

> Nix is currently only supported on macOS and linux.

### Using rustup

> If you plan on using nix, you can skip the installation step, as the nix
> devshell already provides it.

Follow [these instructions](https://www.rust-lang.org/tools/install) to install
rustup.

#### Building with cargo

You can build one of albedo's crates by running `cargo build <crate-name>`. See
the [list of libraries](#libraries) and [list of apps](#apps) for all the
available crates.

Running `cargo build` will build all crates.

#### Testing with cargo

Running `cargo test <crate-name>` will run all the crate's tests. For finer
control, refer to the
[cargo book](https://doc.rust-lang.org/cargo/commands/cargo-test.html).

### Using nix

> If you don't plan on using nix, you can skip this step.

```
# Download installation script
curl -L https://nixos.org/nix/install > install-nix.sh
# Install nix in multi-user (recommended)
install-nix.sh --daemon
# Or in single-user
# install-nix.sh
```

Albedo uses a beta feature of nix (flakes). To enable it, add the following in
`~/.config/nix/nix.conf` or `/etc/nix/nix.conf`:

```
experimental-features = nix-command flakes
```

Finally, if the Nix installation is in multi-user mode, restart the nix-daemon:
`systemctl restart nix-daemon`.

#### The nix devshell

Albedo provides a [devshell](https://github.com/numtide/devshell), a
development environment in a bash shell that allows you to have a working setup
in a split of a second.

You can enter the devshell by running `nix develop`. This will print a summary
of the main commands available in the devshell.

You may run commands inside the devshell by running `nix develop -c <cmd>`. For
instance, `nix develop -c cargo build` will build all of albedo's crates with
cargo provided you have the repository cloned and nix installed (rustup and
cargo will be installed on the fly if needed !).

#### Building with nix

If you have the repository cloned, you may build one of albedo's crates by
running `nix build path/to/albedo/repo#<crate-name>` (the `#` may need to be
escaped depending on your shell).

If you don't have the repository cloned, run
`nix build gitlab:austreelis/albedo#<crate-name>`.
You may specify a specific git ref to build like so:
`nix build gitlab:austreelis/albedo/6b9247#<crate-name>`, where the ref can be
a tag, branch name or commit hash.

By default the crate is built in release mode, to build the debug version,
append `-debug` to the crate's name.

> Note: Nix builds are currently broken. This is being investigated.

#### Testing with nix

Running `nix flake check <crate-name>-tests` will run all the crate's tests.

## License

All source code of the Albedo project is licensed under the GNU General Public
License version 3+.
