use crate::{Cores, Ship, ShipCore};
use albedo_modules::{Hull, FSD};
use albedo_types::ship_generics::Adder;

impl<HL, FD> Ship for ShipCore<Cores<HL, FD>, (), (), (), ()>
where
    HL: Hull<Ship = Adder>,
    FD: FSD,
{
    type Hull = HL;
    type FSD = FD;

    const NAME: &'static str = "Adder";
    const CREW_SEATS: u64 = 2;
    const TOP_SPEED: f64 = 220.0;
    const BOOST_SPEED: f64 = 320.0;
    const MIN_THRUST: f64 = 0.4545;
    const BOOST_COST: f64 = 8.0;
    const MANOEUVRABILITY: u64 = 4;
    const PITCH_SPEED: f64 = 38.0;
    const ROLL_SPEED: f64 = 14.0;
    const YAW_SPEED: f64 = 100.0;
    const MIN_PITCH_SPEED: f64 = 30.0;
    const SHIELDS: u64 = 60;
    const ARMOUR: u64 = 90;
    const ARMOUR_HARDNESS: u64 = 35;
    const HEAT_CAPACITY: u64 = 170;
    const MIN_HEAT_DISSIPATION: f64 = 1.45;
    const MAX_HEAT_DISSIPATION: f64 = 22.6;
    const MASS: f64 = 35.0;
    const MASS_LOCK: u64 = 7;
    const FUEL_COST: u64 = 50;
    const FUEL_RESERVE: f64 = 0.36;

    fn hull(&self) -> &Self::Hull {
        &self.cores.hull
    }

    fn fsd(&self) -> &Self::FSD {
        &self.cores.fsd
    }
}
