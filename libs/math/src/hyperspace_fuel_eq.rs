//! This module handles fuel and range computations using the
//! [hyperspace fuel equation](https://www.reddit.com/r/EliteDangerous/comments/30nx4u/the_hyperspace_fuel_equation_documented/)
//! :
//!
//! ```
//! Fj = l * ( Dj * m / Om ) ^ p
//! ```
//!
//! Where:
//!
//! - `Fj` is the fuel consumed in a hyperspace jump.
//! - `l` is a constant depending on the ship's FSD `Class`:
//!     - `A`: `0.012`
//!     - `B`: `0.01`
//!     - `C`: `0.008`
//!     - `D`: `0.01`
//!     - `E`: `0.011`
//! - `Dj` is the jump distance in Light Years.
//! - `m` is the ship's mass in Tons.
//! - `Om` is the FSD's optimal mass, this is a constant depending on the FSD's `Class` and size.
//! - `p` is a constant depending on the FSD size:
//!     - `2`: `2.0`
//!     - `3`: `2.15`
//!     - `4`: `2.3`
//!     - `5`: `2.45`
//!     - `6`: `2.6`
//!     - `7`: `2.75`
//!
//! It's worth noting a few things.
//!
//! First, fuel consumption is a function of the distance and ship's mass product `Jd * m`. This
//! means that in order to keep the fuel consumption constant when increasing the jump distance
//! a given factor, one must decrease its ship's mass by the same factor.
//!
//! Second, the fuel equation is an exponential relation between the fuel consumed `Fj` and the
//! distance and ship's mass product `Jd * m`. This means decreasing or increasing those value will
//! dramatically increase or dramatically decrease, respectively, fuel consumption. On the other
//! hand, the equation is an inverse exponential relation between `Fj` and the ship's FSD's optimal
//! mass `Om`, which means decreasing or increasing this value will dramatically decrease or
//! dramatically increase, respectively, fuel consumption.
//!
//! Sources:
//! - [ED Shipyard](http://www.edshipyard.com/)
//! - [starchart.club](http://starchart.club/about/)

use albedo_utils::consts::fsd::FSD_CONSTS;
use albedo_utils::types::core_modules::FSDSpec;

#[inline]
fn lin_const(class: u8) -> f64 {
    match class {
        65 /* 'A' */ => 0.012f64,
        66 /* 'B' */ => 0.01,
        67 /* 'C' */ => 0.008,
        68 /* 'D' */ => 0.01,
        69 /* 'E' */ => 0.011,
        _ => unreachable!(),
    }
}

#[inline]
fn pow_const(size: u8) -> f64 {
    1.7 + 0.15 * size as f64
}

#[inline]
fn normal_eq(l: f64, dj: f64, m: f64, om: f64, p: f64) -> f64 {
    l * (dj * (m / om)).powf(p)
}

#[inline]
fn inverse_eq(l: f64, fj: f64, d: f64, om: f64, p: f64) -> f64 {
    #[cfg(not(feature = "optimized-float-accuracy"))]
    {
        om / d * (fj / l).powf(1 / p)
    }
    #[cfg(feature = "optimized-float-accuracy")]
    {
        om * ((fj.ln() + l.recip().ln()) / p).exp() / d
    }
}

#[inline]
pub fn jump_fuel(spec: FSDSpec, dist: f64, ship_mass: f64) -> f64 {
    normal_eq(
        lin_const(spec.class()),
        dist,
        ship_mass,
        FSD_CONSTS.optimized_mass(&spec),
        pow_const(spec.size()),
    )
}

#[inline]
pub fn jump_dist(spec: FSDSpec, fuel_consumed: f64, ship_mass: f64) -> f64 {
    inverse_eq(
        lin_const(spec.class()),
        fuel_consumed,
        ship_mass,
        FSD_CONSTS.optimized_mass(&spec),
        pow_const(spec.size()),
    )
}

#[inline]
pub fn ship_mass_nominal(spec: FSDSpec, fuel_consumed: f64, range: f64) -> f64 {
    inverse_eq(
        lin_const(spec.class()),
        fuel_consumed,
        range,
        FSD_CONSTS.optimized_mass(&spec),
        pow_const(spec.size()),
    )
}

#[inline]
pub fn ship_mass_almost_dry(spec: FSDSpec, max_range: f64, cur_range: f64) -> f64 {
    ship_dry_mass(spec, max_range) + ship_fuel_left_almost_dry(spec, max_range, cur_range)
}

#[inline]
pub fn ship_dry_mass(spec: FSDSpec, max_range: f64) -> f64 {
    let max_fuel_consumption = FSD_CONSTS.max_fuel_consumption(&spec);
    ship_mass_nominal(spec, max_fuel_consumption, max_range) - max_fuel_consumption
}

#[inline]
pub fn ship_fuel_left_nominal(spec: FSDSpec, max_range: f64, cur_range: f64) -> f64 {
    FSD_CONSTS.max_fuel_consumption(&spec) + ship_dry_mass(spec, cur_range)
        - ship_dry_mass(spec, max_range)
}

#[inline]
pub fn ship_fuel_left_almost_dry(spec: FSDSpec, max_range: f64, cur_range: f64) -> f64 {
    // This computation is actually an approximation, assuming the ship mass is large enough
    // compared to the result (the fuel left in the tank).
    // This approximation is more accurate (according to Herbie) than the real solution if
    // Om / (l^(1/p) * Dj) < 4.35e28, which is practically always the case
    normal_eq(
        lin_const(spec.class()),
        cur_range,
        ship_dry_mass(spec, max_range),
        FSD_CONSTS.optimized_mass(&spec),
        pow_const(spec.size()),
    )
}
