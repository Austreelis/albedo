#[derive(Copy, Clone)]
pub struct Upgrade<U, const G: u8, E> {
    _upgrade: U,
    _effect: E,
}

pub mod upgrade {
    #[derive(Copy, Clone, Eq, PartialEq)]
    pub struct Unengineered;
    #[derive(Copy, Clone, Eq, PartialEq)]
    pub struct Lightweight;
}

pub mod experimental {
    #[derive(Copy, Clone, Eq, PartialEq)]
    pub struct PlasmaSlug;
    #[derive(Copy, Clone, Eq, PartialEq)]
    pub struct StrippedDown;
    #[derive(Copy, Clone, Eq, PartialEq)]
    pub struct ThermalSpread;
}

#[cfg(test)]
mod tests {
    #[test]
    fn upgrade_is_zst() {
        assert_eq!(0, std::mem::size_of::<super::Upgrade<(), 1, ()>>());
        assert_eq!(0, std::mem::size_of::<super::Upgrade<(), 2, ()>>());
        assert_eq!(0, std::mem::size_of::<super::Upgrade<(), 3, ()>>());
        assert_eq!(0, std::mem::size_of::<super::Upgrade<(), 4, ()>>());
        assert_eq!(0, std::mem::size_of::<super::Upgrade<(), 5, ()>>());
    }
}
