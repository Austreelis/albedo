use crate::Module;
use albedo_traits::Mass;
use albedo_types::ship_generics::Adder;

pub trait Hull {
    /// The type of ship this hull is for.
    type Ship: Copy + Default;

    /// The mass of the hull module in Tons.
    const MASS: f64;

    /// Hull strength bonus.
    const HULL_BOOST: f64;

    /// Hull strength bonus/malus against kinetic damage.
    const KINETIC_RESISTANCE: f64;

    /// Hull strength bonus/malus against thermal damage.
    const THERMAL_RESISTANCE: f64;

    /// Hull strength bonus/malus against explosive damage.
    const EXPLOSIVE_RESISTANCE: f64;

    /// Hull strength bonus/malus against corrosive damage.
    const CORROSIVE_RESISTANCE: f64;
}

impl<S, U> Mass for Module<LightWeight<S>, U>
where
    Module<LightWeight<S>, U>: Hull,
{
    const MASS: f64 = <Self as Hull>::MASS;

    fn mass(&self) -> f64 {
        <Self as Mass>::MASS
    }
}

#[derive(Copy, Clone, Default)]
pub struct LightWeight<S>(pub S);

#[derive(Copy, Clone, Default)]
pub struct MilitaryGrade<S>(pub S);

impl Hull for Module<LightWeight<Adder>, ()> {
    type Ship = Adder;
    const MASS: f64 = 0.0;
    const HULL_BOOST: f64 = 0.8;
    const KINETIC_RESISTANCE: f64 = -0.2;
    const THERMAL_RESISTANCE: f64 = 0.0;
    const EXPLOSIVE_RESISTANCE: f64 = -0.4;
    const CORROSIVE_RESISTANCE: f64 = 0.9;
}

impl Hull for Module<MilitaryGrade<Adder>, ()> {
    type Ship = Adder;
    const MASS: f64 = 5.0;
    const HULL_BOOST: f64 = 2.5;
    const KINETIC_RESISTANCE: f64 = -0.2;
    const THERMAL_RESISTANCE: f64 = 0.0;
    const EXPLOSIVE_RESISTANCE: f64 = -0.4;
    const CORROSIVE_RESISTANCE: f64 = 0.9;
}
