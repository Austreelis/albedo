// TODO: Migrate to a proper type-checked unit system.
// TODO: use feature associated_type_defaults when it's in a usable state
// FIXME: Strengthen constraint to numtrait's Real + Eq + Ord or another custom trait. This will
//  likely require a wrapper type and may be kind of a big undertaking ?
pub trait Mass<U = f64>
where
    U: Copy,
{
    /// The base mass
    const MASS: f64;

    /// The current mass of the entity, with any modifier applied on top of
    /// [`Self::MASS`](self::Mass::MASS).
    fn mass(&self) -> U;
}
