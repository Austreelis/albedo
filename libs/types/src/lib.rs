use std::fmt;
use std::fmt::{Debug, Display};
use std::ops::Add;

/// A structure representing the state of a [`PowerDistributor`][PD]'s settings.
///
/// Pips are a simple way to represent dynamic power allocation. The power distributor draws
/// constant power from the ship's power plant, and redistributes it to the modules that allows
/// being plugged in. The power consumption of modules plugged into the power distributor can then
/// be dynamically allocated to three different groups of modules by allocating a fixed number of
/// pips. Each pip in a group represent the same amount of power, but each group may have different
/// total power capacity, and the precise amount they represent depends on
/// [`Pips::MAX_CAP`](crate::Pips::MAX_CAP) and the power distributor's power capacity for that
/// group.
///
/// Note modules feeding on the power distributor still draw a base constant power from the power
/// plant.
///
/// [PD]: albedo_modules::PowerDistributor
#[repr(C)]
#[derive(Copy, Clone, Eq, PartialEq)]
pub struct Pips<const T: u16> {
    /// The number of pips allocated to systems. This includes shields, shield cell banks and some
    /// utility modules.
    sys: u8,
    /// The number of pips allocated to engine (i.e. the thrusters).
    eng: u8,
    /// The number of pips allocated to weapons. This includes most weapons.
    wep: u8,
}

impl<const T: u16> Default for Pips<T> {
    fn default() -> Self {
        Pips {
            sys: 2,
            eng: 2,
            wep: 2,
        }
    }
}

impl<const T: u16> Pips<T> {
    /// The maximum amount of pips a group may be allocated.
    pub const MAX_CAP: u8 = 8;

    /// Creates a new `Pips` object.
    ///
    /// # Panics
    ///
    /// Panics if any of the arguments exceeds [`Self::MAX_CAP`](crate::Pips::MAX_CAP), or if the
    /// sum of all pip values is not equal to the total capacity `T`.
    #[inline]
    pub fn new(sys: u8, eng: u8, wep: u8) -> Self {
        if sys > Self::MAX_CAP {
            panic!(
                "attempt to create pips with sys of {} exceeding Pips::Max_CAP of {}",
                sys,
                Self::MAX_CAP
            );
        }
        if eng > Self::MAX_CAP {
            panic!(
                "attempt to create pips with eng of {} exceeding Pips::Max_CAP of {}",
                eng,
                Self::MAX_CAP
            );
        }
        if wep > Self::MAX_CAP {
            panic!(
                "attempt create pips with wep of {} exceeding Pips::Max_CAP of {}",
                wep,
                Self::MAX_CAP
            );
        }
        let total = sys as u16 + eng as u16 + wep as u16;
        if total != T {
            panic!(
                "attempt to create pips with total of {} different than actual total capacity {}",
                sys, T
            );
        }
        Pips { sys, eng, wep }
    }
}

impl<const T: u16> Add<(u8, u8, u8)> for Pips<T> {
    type Output = Pips<T>;

    /// Performs the + operation.
    ///
    /// # Panics
    ///
    /// Panics if any pip value is invalid for the same reasons as [`new()`](crate::Pips::new).
    fn add(self, rhs: (u8, u8, u8)) -> Self::Output {
        let (sys, eng, wep) = rhs;
        let pips = Pips {
            sys: self.sys + sys,
            eng: self.eng + eng,
            wep: self.wep + wep,
        };
        pips
    }
}

impl<const T: u16> Debug for Pips<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[Sys: {}/{}; Eng: {}/{}; Wep: {}/{}]",
            self.sys,
            Self::MAX_CAP,
            self.eng,
            Self::MAX_CAP,
            self.wep,
            Self::MAX_CAP
        )
    }
}

impl<const T: u16> Display for Pips<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[Sys ")?;
        for i in 0..Self::MAX_CAP {
            if i < self.sys {
                write!(f, "|")?;
            } else {
                write!(f, ".")?;
            }
        }
        write!(f, "  Eng ")?;
        for i in 0..Self::MAX_CAP {
            if i < self.eng {
                write!(f, "|")?;
            } else {
                write!(f, ".")?;
            }
        }
        write!(f, "  Wep ")?;
        for i in 0..Self::MAX_CAP {
            if i < self.wep {
                write!(f, "|")?;
            } else {
                write!(f, ".")?;
            }
        }
        write!(f, "]")
    }
}

/// Companies that manufacture ships, modules, weapons, gears, suits, ...
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Manufacturers {
    CoreDynamics,
    FaulconDeLacy,
    Gutamaya,
    Lakon,
    SaudKruger,
    ZorgonPeterson,
}

impl Display for Manufacturers {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(match self {
            Manufacturers::CoreDynamics => "Core Dynamics",
            Manufacturers::FaulconDeLacy => "Faulcon DeLacy",
            Manufacturers::Gutamaya => "Gutamaya",
            Manufacturers::Lakon => "Lakon Spaceways",
            Manufacturers::SaudKruger => "Saud Kruger",
            Manufacturers::ZorgonPeterson => "Zorgon Peterson",
        })
    }
}

/// A collection of zero-sized types representing ships, to be used as generics arguments.
pub mod ship_generics {
    #[derive(Copy, Clone, Default)]
    pub struct Adder;
}
