use crate::types::core_modules::FSDSpec;
use once_cell::unsync::Lazy;
use std::collections::HashMap;

pub const FSD_2D_MASS: f64 = 1.;
pub const FSD_2A_MASS: f64 = 2.5;
pub const FSD_3D_MASS: f64 = 2.;
pub const FSD_3A_MASS: f64 = 5.;
pub const FSD_4D_MASS: f64 = 4.;
pub const FSD_4A_MASS: f64 = 10.;

pub const FSD_2D_MAX_FUEL_CONSUMPTION: f64 = 0.6;
pub const FSD_2A_MAX_FUEL_CONSUMPTION: f64 = 0.9;
pub const FSD_3D_MAX_FUEL_CONSUMPTION: f64 = 1.2;
pub const FSD_3A_MAX_FUEL_CONSUMPTION: f64 = 1.8;
pub const FSD_4D_MAX_FUEL_CONSUMPTION: f64 = 2.0;
pub const FSD_4A_MAX_FUEL_CONSUMPTION: f64 = 3.0;

pub const FSD_2D_OPTIMIZED_MASS: f64 = 54.;
pub const FSD_2A_OPTIMIZED_MASS: f64 = 90.;
pub const FSD_3D_OPTIMIZED_MASS: f64 = 90.;
pub const FSD_3A_OPTIMIZED_MASS: f64 = 150.;
pub const FSD_4D_OPTIMIZED_MASS: f64 = 315.;
pub const FSD_4A_OPTIMIZED_MASS: f64 = 525.;

struct FSDConsts {
    pub mass: f64,
    pub max_fuel_consumption: f64,
    pub optimized_mass: f64,
}

pub struct FSDConstsMap {
    inner: HashMap<FSDSpec, FSDConsts>,
}

impl FSDConstsMap {
    pub fn mass(&self, spec: &FSDSpec) -> f64 {
        // TODO: Add a test to ensure these unwraps never panic
        self.inner.get(spec).unwrap().mass
    }

    pub fn max_fuel_consumption(&self, spec: &FSDSpec) -> f64 {
        self.inner.get(spec).unwrap().max_fuel_consumption
    }

    pub fn optimized_mass(&self, spec: &FSDSpec) -> f64 {
        self.inner.get(spec).unwrap().optimized_mass
    }
}

pub const FSD_CONSTS: Lazy<FSDConstsMap> = Lazy::new(|| {
    let mut map = HashMap::new();
    map.insert(
        FSDSpec {
            size: 2,
            class: 'D' as u8,
        },
        FSDConsts {
            mass: FSD_2D_MASS,
            max_fuel_consumption: FSD_2D_MAX_FUEL_CONSUMPTION,
            optimized_mass: FSD_2D_OPTIMIZED_MASS,
        },
    );
    map.insert(
        FSDSpec {
            size: 2,
            class: 'A' as u8,
        },
        FSDConsts {
            mass: FSD_2A_MASS,
            max_fuel_consumption: FSD_2A_MAX_FUEL_CONSUMPTION,
            optimized_mass: FSD_2A_OPTIMIZED_MASS,
        },
    );
    map.insert(
        FSDSpec {
            size: 3,
            class: 'D' as u8,
        },
        FSDConsts {
            mass: FSD_3D_MASS,
            max_fuel_consumption: FSD_3D_MAX_FUEL_CONSUMPTION,
            optimized_mass: FSD_3D_OPTIMIZED_MASS,
        },
    );
    map.insert(
        FSDSpec {
            size: 3,
            class: 'A' as u8,
        },
        FSDConsts {
            mass: FSD_3A_MASS,
            max_fuel_consumption: FSD_3A_MAX_FUEL_CONSUMPTION,
            optimized_mass: FSD_3A_OPTIMIZED_MASS,
        },
    );
    map.insert(
        FSDSpec {
            size: 4,
            class: 'D' as u8,
        },
        FSDConsts {
            mass: FSD_4D_MASS,
            max_fuel_consumption: FSD_4D_MAX_FUEL_CONSUMPTION,
            optimized_mass: FSD_4D_OPTIMIZED_MASS,
        },
    );
    map.insert(
        FSDSpec {
            size: 4,
            class: 'A' as u8,
        },
        FSDConsts {
            mass: FSD_4A_MASS,
            max_fuel_consumption: FSD_4A_MAX_FUEL_CONSUMPTION,
            optimized_mass: FSD_4A_OPTIMIZED_MASS,
        },
    );
    FSDConstsMap { inner: map }
});
