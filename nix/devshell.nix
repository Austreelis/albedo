{ lib, pkgs, ... }:

with lib;

let

  rustup = lowPrio pkgs.rustup;

in {
  # Add a low-prio rustup. Avoids conflict with other packages like
  # clippy in the devshell env
  # Some IDEs seems to have a hard time working with rust-overlay's
  # toolchains
  # TODO: Investigate above comment
  commands = [
    { name = "rustup"; category = "rust"; package = rustup; }
  ];
}
