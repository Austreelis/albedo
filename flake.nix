{
  inputs = {
    nixpkgs.url = "nixpkgs";
    nixCargoIntegration = {
      url = "github:yusdacra/nix-cargo-integration";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { ... } @ inputs:
    let
      crate = {
        buildPlatform = "crate2nix";
        root = ./.;
      };
    in
    inputs.nixCargoIntegration.lib.makeOutputs crate;
}
